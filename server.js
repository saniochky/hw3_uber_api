const config = require('config');
const express = require('express');
const mongoose = require('mongoose');
const winston = require('winston');
const expressWinston = require('express-winston');

const app = express();
const PORT = config.get('Customer.dbConfig.port') || 8080;
const dbPassword = config.get('Customer.dbConfig.dbPassword');

app.use(express.json({type: '*/*'}));

app.use(expressWinston.logger({
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json(),
    ),
    meta: true,
    msg: 'HTTP {{req.method}} {{req.url}}',
    expressFormat: true,
    colorize: false,
    ignoreRoute: function(req, res) {
        return false;
    },
}));

app.use('/api/auth', require('./routes/auth'));
app.use('/api/users/me', require('./routes/users'));
app.use('/api/trucks', require('./routes/trucks'));
app.use('/api/loads', require('./routes/loads'));

async function startServer() {
    try {
        await mongoose.connect(`mongodb+srv://artdance:${dbPassword}@cluster0.vxtvu.mongodb.net/hw2-db`, {
            useNewUrlParser: true,
        });

        app.listen(PORT);
    } catch (error) {
        console.log(error.message);
    }
}

startServer();
