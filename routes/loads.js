const express = require('express');
const loadSchema = require('../models/load');
const validateJWT = require('../validation/jwt_validation');
const loadValidation = require('../validation/load_validation');
const shipperValidation = (req, res, next) => require('../validation/driver_shipper_validation')(req, res, next, shipper);
const driverValidation = (req, res, next) => require('../validation/driver_shipper_validation')(req, res, next, driver);
const stateGenerator = require('../generators/state_generator');
const truckSchema = require("../models/truck");

const router = express.Router();
const driver = 'DRIVER';
const shipper = 'SHIPPER';

router.use(validateJWT);

router.route('/')
    .get(getLoads, (req, res) => {
        res.json({
            'loads': req.loads,
        });
    })
    .post(shipperValidation, validateLoadBody, addLoad, (rew, res) => {
        res.json({
            'message': 'Load created successfully',
        });
    });

router.get('/active', driverValidation, getActiveLoads, (req, res) => {
    res.json({
        'load': req.load,
    });
});

router.patch('/active/state', driverValidation, switchState, (req, res) => {
    res.json({
        'message': `Load state changed to '${req.state}'`,
    });
});

router.route('/:id')
    .get((req, res) => {
        res.json({
            'load': {
                '_id': req.load._id,
                'created_by': req.load.created_by,
                'assigned_to': req.load.assigned_to,
                'status': req.load.status,
                'state': req.load.state,
                'name': req.load.name,
                'payload': req.load.payload,
                'pickup_address': req.load.pickup_address,
                'delivery_address': req.load.delivery_address,
                'dimensions': req.load.dimensions,
                'logs': req.load.logs,
                'created_date': req.load.created_date,
            }
        });
    })
    .put(shipperValidation, validateLoadBody, updateLoadById, (req, res) => {
        res.json({
            'message': 'Load details changed successfully',
        });
    })
    .delete(shipperValidation, deleteLoadById, (req, res) => {
        res.json({
            'message': 'Load deleted successfully',
        });
    });

router.post('/:id/post', shipperValidation, postLoad, (req, res) => {
    res.json({
        'message': 'Load posted successfully',
        'driver_found': req.driver_found,
    });
});

router.get('/:id/shipping_info', shipperValidation, getShipmentInfoById, (req, res) => {
    res.json({
        'load': req.load,
        'truck': req.truck,
    });
});

router.param('id', async (req, res, next, loadId) => {
    try {
        const load = await loadSchema.findById(loadId);

        if (load) {
            req.load = load;
            next();
        } else {
            res.status(400).json({
                'message': `Load with id '${loadId}' doesn't exist`,
            });
        }
    } catch (error) {
        res.status(400).json({
            'message': `Load with id '${loadId}' doesn't exist`,
        });
    }
});

async function getLoads(req, res, next) {
    try {
        let loads;

        if (req.role === driver) {
            loads = await loadSchema.find({'assign_to': req.userId});
        } else {
            loads = await loadSchema.find({'created_by': req.userId});
        }

        req.loads = loads.map((load) => {
            return (({
                         _id,
                         created_by,
                         assigned_to,
                         status,
                         state,
                         name,
                         payload,
                         pickup_address,
                         delivery_address,
                         dimensions,
                         logs,
                         created_date,
                     }) => ({
                _id,
                created_by,
                assigned_to,
                status,
                state,
                name,
                payload,
                pickup_address,
                delivery_address,
                dimensions,
                logs,
                created_date,
            }))(load);
        });
        next();
    } catch (error) {
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
}

async function getActiveLoads(req, res, next) {
    try {
        const load = await loadSchema.findOne({'assign_to': req.userId, 'status': 'ASSIGNED'});
        req.load = (({
                         _id,
                         created_by,
                         assigned_to,
                         status,
                         state,
                         name,
                         payload,
                         pickup_address,
                         delivery_address,
                         dimensions,
                         logs,
                         created_date,
                     }) => ({
            _id,
            created_by,
            assigned_to,
            status,
            state,
            name,
            payload,
            pickup_address,
            delivery_address,
            dimensions,
            logs,
            created_date,
        }))(load);
        next();
    } catch (error) {
        req.load = {};
    }
    next();
}

function validateLoadBody(req, res, next) {
    const body = loadValidation.validate(req.body);

    if (body.error) {
        res.status(400).json({
            'message': body.error.message,
        });
    } else {
        next();
    }
}

async function addLoad(req, res, next) {
    try {
        const newLoad = new loadSchema({
            'created_by': req.userId,
            'name': req.body.name,
            'payload': req.body.payload,
            'pickup_address': req.body.pickup_address,
            'delivery_address': req.body.delivery_address,
            'dimensions': req.body.dimensions,
        });
        await newLoad.save();
        next();
    } catch (error) {
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
}

async function switchState(req, res, next) {
    try {
        const load = await loadSchema.findOne({'assign_to': req.userId, 'status': 'ASSIGNED'});

        if (load) {
            const newState = stateGenerator[load.state];

            if (newState) {
                req.state = newState;
                load.state = newState;
                load.logs.push({
                    'message': `Truck ${newState.toLowerCase()}`,
                });

                if (!stateGenerator[newState]) {
                    const truck = await truckSchema.findOne({'assign_to': load.assigned_to});
                    truck.status = 'IS';
                    load.status = 'SHIPPED';
                    await truck.save();

                }
                await load.save();
                next();
            } else {
                res.status(400).json({
                    'message': 'This load is already arrived to delivery',
                });
            }
        } else {
            res.status(400).json({
                'message': 'You don\'t have assigned load',
            });
        }
    } catch (error) {
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
}

async function updateLoadById(req, res, next) {
    try {
        if (req.load.status !== 'ASSIGNED' && req.load.status !== 'SHIPPED') {
            req.load.name = req.body.name;
            req.load.payload = req.body.payload;
            req.load.pickup_address = req.body.pickup_address;
            req.load.delivery_address = req.body.delivery_address;
            req.load.dimensions = req.body.dimensions;
            await req.load.save();
            next()
        } else {
            res.status(400).json({
                'message': 'You cannot update this load, since it has already been assigned or shipped',
            });
        }
    } catch (error) {
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
}

async function deleteLoadById(req, res, next) {
    try {
        if (req.load.status !== 'ASSIGNED') {
            await loadSchema.deleteOne({'_id': req.load._id});
            next();
        } else {
            res.status(400).json({
                'message': 'You cannot delete this load since it is already assigned',
            });
        }
    } catch (error) {
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
}

async function postLoad(req, res, next) {
    try {
        if (req.load.status !== 'NEW') {
            res.status(400).json({
                'message': 'This load has already been posted',
            });
        } else {
            req.load.status = 'POSTED';
            const availableTruck = await truckSchema.findOne({'status': 'IS', 'assigned_to': {$ne: null}});

            if (availableTruck) {
                availableTruck.status = 'OL';
                await availableTruck.save();
                req.load.assigned_to = availableTruck.assigned_to;
                req.load.status = 'ASSIGNED';
                req.load.state = 'En route to Pick Up';
                req.load.logs.push({
                    'message': `Load assigned to driver with id '${availableTruck.assigned_to}'`,
                });
                req.driver_found = true;
            } else {
                req.driver_found = false;
            }

            await req.load.save();
            next();
        }
    } catch (error) {
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
}

async function getShipmentInfoById(req, res, next) {
    try {
        if (req.load.status !== 'SHIPPED') {
            req.load = {
                '_id': req.load._id,
                'created_by': req.load.created_by,
                'assigned_to': req.load.assigned_to,
                'status': req.load.status,
                'state': req.load.state,
                'name': req.load.name,
                'payload': req.load.payload,
                'pickup_address': req.load.pickup_address,
                'delivery_address': req.load.delivery_address,
                'dimensions': req.load.dimensions,
                'logs': req.load.logs,
                'created_date': req.load.created_date,
            };

            if (req.load.assigned_to) {
                const truck = await truckSchema.findOne({'assigned_to': req.load.assigned_to});
                req.truck = {
                    '_id': truck._id,
                    'created_by': truck.created_by,
                    'assigned_to': truck.assigned_to,
                    'type': truck.type,
                    'status': truck.status,
                    'created_date': truck.created_date,
                };
            } else {
                req.truck = {};
            }
        } else {
            req.load = {};
        }
        next();
    } catch (error) {
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
}

module.exports = router;
