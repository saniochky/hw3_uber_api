const bcrypt = require('bcrypt');
const config = require('config');
const express = require('express');
const jwt = require('jsonwebtoken');
const emailValidation = require('../validation/email_validation');
const loginValidation = require('../validation/login_validation');
const registerValidation = require('../validation/register_validation');
const userSchema = require('../models/user');

const saltRounds = 12;
const secretKey = config.get('Customer.dbConfig.secretKey');
const router = express.Router();

router.post('/register', checkRegisterBody, register, (req, res) => {
    res.json({
        'message': 'Profile created successfully',
    });
});

router.post('/login', checkLoginBody, login, (req, res) => {
    res.json({
        'jwt_token': req.jwt,
    });
});

router.post('/forgot_password', checkForgotPasswordBody, sendNewPassword, (req, res) => {
    res.json({
        'message': 'Success',
    });
});

function checkRegisterBody(req, res, next) {
    const body = registerValidation.validate(req.body);

    if (body.error) {
        res.status(400).json({
            'message': body.error.message,
        });
    } else {
        next();
    }
}

async function register(req, res, next) {
    try {
        const user = await userSchema.findOne({'email': req.body.email});

        if (user) {
            res.status(400).json({
                message: `User with email ${req.body.email} already exists`,
            });
        } else {
            const newUser = new userSchema({
                email: req.body.email,
                encryptedPassword: bcrypt.hashSync(req.body.password, saltRounds),
                role: req.body.role,
            });

            await newUser.save();
            next();
        }
    } catch (error) {
        res.status(500).json({
            message: 'Internal server error',
        });
    }
}

function checkLoginBody(req, res, next) {
    const body = loginValidation.validate(req.body);

    if (body.error) {
        res.status(400).json({
            'message': body.error.message,
        });
    } else {
        next();
    }
}

async function login(req, res, next) {
    try {
        const user = await userSchema.findOne({'email': req.body.email});

        if (!user) {
            res.status(400).json({
                'message': `User with email ${req.body.email} doesn\'t exist`,
            });
        } else if (!bcrypt.compareSync(req.body.password, user.encryptedPassword)) {
            res.status(400).json({
                'message': 'Password is incorrect',
            });
        } else {
            req.userId = user._id;
            req.jwt = jwt.sign({
                _id: req.userId,
                role: user.role,
                encryptedPassword: user.encryptedPassword
            }, secretKey);
            next();
        }
    } catch (error) {
        res.status(500).json({
            message: 'Internal server error',
        });
    }
}

function checkForgotPasswordBody(req, res, next) {
    const body = emailValidation.validate(req.body);

    if (body.error) {
        res.status(400).json({
            'message': body.error.message,
        });
    } else {
        next();
    }
}

async function sendNewPassword(req, res, next) {
    try {
        const user = await userSchema.findOne({'email': req.body.email});

        if (!user) {
            res.status(400).json({
                'message': `User with email ${req.body.email} doesn\'t exist`,
            });
        } else {
            const newPassword = (Math.random() + 1).toString(36).substring(2);
            console.log(newPassword);

            user.encryptedPassword = bcrypt.hashSync(newPassword, saltRounds);
            await user.save();
            next();
        }
    } catch (error) {
        res.status(500).json({
            message: 'Internal server error',
        });
    }
}

module.exports = router;
