const express = require('express');
const truckSchema = require('../models/truck');
const validateJWT = require('../validation/jwt_validation');
const driverValidation = require('../validation/driver_shipper_validation');
const typeValidation = require('../validation/type_validation');

const role = 'DRIVER';
const router = express.Router();

router.use(validateJWT);
router.use((req, res, next) => driverValidation(req, res, next, role));

router.route('/')
    .get(getTrucks, (req, res) => {
        res.json({
            'trucks': req.trucks,
        });
    })
    .post(validateType, addTruck, (req, res) => {
        res.json({
            'message': 'Truck created successfully',
        });
    });

router.route('/:id')
    .get((req, res) => {
        res.json({
            'truck': {
                '_id': req.truck._id,
                'created_by': req.truck.created_by,
                'assigned_to': req.truck.assigned_to,
                'type': req.truck.type,
                'status': req.truck.status,
                'created_date': req.truck.created_date,
            },
        });
    })
    .put(validateType, updateTruckById, (req, res) => {
        res.json({
            'message': 'Truck details changed successfully',
        });
    })
    .delete(deleteTruckById, (req, res) => {
        res.json({
            'message': 'Truck deleted successfully',
        });
    });

router.post('/:id/assign', assignTruckById, (req, res) => {
    res.json({
        'message': 'Truck assigned successfully',
    });
});

router.param('id', async (req, res, next, truckId) => {
    try {
        const truck = await truckSchema.findById(truckId);

        if (truck) {
            req.truck = truck;
            next();
        } else {
            res.status(400).json({
                'message': `Truck with id '${truckId}' doesn't exist`,
            });
        }
    } catch (error) {
        res.status(400).json({
            'message': `Truck with id '${truckId}' doesn't exist`,
        });
    }
});

async function getTrucks(req, res, next) {
    try {
        const trucks = await truckSchema.find({'created_by': req.userId});
        req.trucks = trucks.map((truck) => {
            return {
                '_id': truck._id,
                'created_by': truck.created_by,
                'assigned_to': truck.assigned_to,
                'type': truck.type,
                'status': truck.status,
                'created_date': truck.created_date,
            };
        });
        next();
    } catch (error) {
        console.log(error.message);
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
}

function validateType(req, res, next) {
    const body = typeValidation.validate(req.body);

    if (body.error) {
        res.status(400).json({
            'message': body.error.message,
        });
    } else {
        next();
    }
}

async function addTruck(req, res, next) {
    try {
        const newTruck = new truckSchema({
            'created_by': req.userId,
            'type': req.body.type,
        });
        await newTruck.save();
        next();
    } catch (error) {
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
}

async function updateTruckById(req, res, next) {
    try {
        req.truck.type = req.body.type;
        await req.truck.save();
        next();
    } catch (error) {
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
}

async function deleteTruckById(req, res, next) {
    try {
        await truckSchema.deleteOne({'_id': req.truck._id});
        next();
    } catch (error) {
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
}

async function assignTruckById(req, res, next) {
    try {
        const assignedTruck = await truckSchema.findOne({'assigned_to': req.userId, 'status': {$ne: 'SHIPPED'}});

        if (assignedTruck) {
            res.status(400).json({
                'message': 'You are already assigned to a truck',
            });
        } else if (req.truck.assigned_to) {
            res.status(400).json({
                'message': 'Truck is already assigned to someone',
            });
        } else {
            req.truck.assigned_to = req.userId;
            await req.truck.save();
            next();
        }
    } catch (error) {
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
}

module.exports = router;
