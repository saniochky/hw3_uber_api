const bcrypt = require('bcrypt');
const express = require('express');
const userSchema = require('../models/user');
const passwordValidation = require('../validation/password_validation');
const validateJWT = require('../validation/jwt_validation');

const saltRounds = 12;
const router = express.Router();

router.use(validateJWT);

router.route('/')
    .get(getUserInfo, (req, res) => {
        res.json(req.userInfo);
    })
    .delete(deleteProfile, (req, res) => {
        res.json({
            'message': 'Profile deleted successfully',
        });
    });

router.patch('/password', checkBody, changeProfilePassword, (req, res) => {
    res.json({
        'message': 'Password changed successfully',
    });
});

async function getUserInfo(req, res, next) {
    try {
        const user = await userSchema.findById(req.userId);

        if (user) {
            req.userInfo = {
                'user': {
                    '_id': req.userId,
                    'role': user.role,
                    'email': user.email,
                    'createdDate': user.createdDate,
                },
            };

            next();
        } else {
            res.status(400).json({
                'message': 'Token is invalid',
            });
        }
    } catch (error) {
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
}

function checkBody(req, res, next) {
    const body = passwordValidation.validate(req.body);

    if (body.error) {
        res.status(400).json({
            'message': body.error.message,
        });
    } else {
        next();
    }
}

async function changeProfilePassword(req, res, next) {
    try {
        const user = await userSchema.findById(req.userId);

        if (user) {
            if (bcrypt.compareSync(req.body.oldPassword, user.encryptedPassword)) {
                user.encryptedPassword = bcrypt.hashSync(req.body.newPassword, saltRounds);
                await user.save();
                next();
            } else {
                res.status(400).json({
                    'message': 'Password is incorrect',
                });
            }
        } else {
            res.status(400).json({
                'message': 'Token is invalid',
            });
        }
    } catch (error) {
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
}

async function deleteProfile(req, res, next) {
    try {
        await userSchema.deleteOne({'_id': req.userId});
        next();
    } catch (error) {
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
}

module.exports = router;
