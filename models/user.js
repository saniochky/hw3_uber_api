const {Schema, model} = require('mongoose');

const schema = new Schema({
    email: {
        type: String,
        required: true,
    },
    encryptedPassword: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        required: true,
    },
    createdDate: {
        type: String,
        default: new Date(Date.now()).toISOString(),
    },
});

module.exports = model('user', schema);
