const {Schema, model} = require('mongoose');

const schema = new Schema({
    created_by: {
        type: String,
        required: true,
    },
    assigned_to: {
        type: String,
        default: null,
    },
    status: {
        type: String,
        default: 'NEW',
    },
    state: {
        type: String,
        default: null,
    },
    name: {
        type: String,
        required: true,
    },
    payload: {
        type: Number,
        default: 10,
    },
    pickup_address: {
        type: String,
        required: true,
    },
    delivery_address: {
        type: String,
        required: true,
    },
    dimensions: {
        type: {
            _id: false,
            width: {
                type: Number,
                default: 10,
            },
            length: {
                type: Number,
                default: 10,
            },
            height: {
                type: Number,
                default: 10,
            },
        },
    },
    logs: {
        type: [{
            _id: false,
            message: {
                type: String,
                required: true,
            },
            time: {
                type: String,
                default: new Date(Date.now()).toISOString(),
            },
        }],
        default: [],
    },
    created_date: {
        type: String,
        default: new Date(Date.now()).toISOString(),
    },
});

module.exports = model('load', schema);
