const {Schema, model} = require('mongoose');

const schema = new Schema({
    created_by: {
        type: String,
        required: true,
    },
    assigned_to: {
        type: String,
        default: null,
    },
    type: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        default: 'IS',
    },
    createdDate: {
        type: String,
        default: new Date(Date.now()).toISOString(),
    },
});

module.exports = model('truck', schema);
