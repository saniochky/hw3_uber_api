const Joi = require('joi');

const schema = Joi.object({
    name: Joi.string()
        .required(),

    payload: Joi.number()
        .greater(0)
        .required(),

    pickup_address: Joi.string()
        .required(),

    delivery_address: Joi.string()
        .required(),

    dimensions: Joi.object({
        width: Joi.number()
            .greater(0)
            .required(),

        length: Joi.number()
            .greater(0)
            .required(),

        height: Joi.number()
            .greater(0)
            .required(),
    })
});

module.exports = schema;
