module.exports = async function validateJWT(req, res, next, role) {
    try {
        if (req.role === role) {
            next();
        } else {
            res.status(400).json({
                'message': `${req.role} cannot perform this action`,
            });
        }
    } catch (error) {
        res.status(500).json({
            'message': 'Internal server error',
        });
    }
};
