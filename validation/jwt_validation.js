const config = require('config');
const jwt = require('jsonwebtoken');
const userSchema = require('../models/user');

const secretKey = config.get('Customer.dbConfig.secretKey');

module.exports = async function validateJWT(req, res, next) {
    const authHeader = req.headers.authorization;

    if (authHeader && authHeader.split(' ')[0] === 'JWT') {
        const token = authHeader.split(' ')[1];

        try {
            const decoded = jwt.verify(token, secretKey);
            const user = await userSchema.findById(decoded._id);

            if (user && user.encryptedPassword === decoded.encryptedPassword) {
                req.userId = decoded._id;
                req.role = user.role;
                next();
            } else {
                res.status(400).json({
                    'message': 'Token is invalid',
                });
            }
        } catch (error) {
            res.status(400).json({
                'message': 'Token is invalid',
            });
        }
    } else {
        res.status(400).json({
            'message': 'Token is missing',
        });
    }
};
